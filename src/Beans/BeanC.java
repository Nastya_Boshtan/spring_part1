package Beans;

public class BeanC implements interfaceBeanValidator{

    private String name;
    private int value;

    public BeanC(String name, int value){
        this.name=name;
        this.value=value;
        System.out.println("c created");
    }
    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate() {
        if(name==null){
            System.out.println("мале ім'я");
        }else if(value<=0){
            System.out.println("value мале");
        }else{
            System.out.println("ok!!!!");
        }
    }

    public void init(){
        System.out.println("BeanC initialized");
    }
    public void destroy(){
        System.out.println("BeanC destroy");
    }
}
