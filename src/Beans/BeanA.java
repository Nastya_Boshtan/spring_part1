package Beans;

import org.springframework.beans.factory.InitializingBean;

public class BeanA implements InitializingBean,DesposableBean, interfaceBeanValidator{

    private String name;
    private int value;

    public BeanA(BeanB beanB, BeanC beanC){
        this.name=beanB.getName();
        this.value=beanC.getValue();

   }
    public String getName() {
        return name;
    }
    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate() {
        if(name==null){
            System.out.println("мале ім'я");
        }else if(value<=0){
            System.out.println("value мале");
        }else{
            System.out.println("ok!!!!");
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("InitializingBean");
    }

    @Override
    public void desposable() {
        System.out.println("DesposableBean");
    }
}
