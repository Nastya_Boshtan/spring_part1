package Beans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;

@Configuration
@Import({BeanСonfigur.class})
public class BeanConfigurImport {
    @Bean("beanA")
    @DependsOn(value={"beanC"})
    @Qualifier("mybeanA")
    public BeanA getBeanA(@Qualifier("mybeanB") BeanB mybean, @Qualifier("mybeanC") BeanC mybeanC){return new BeanA (mybean, mybeanC);}
    @Bean
    public BeanE getBeanE(@Qualifier("mybeanA") BeanA mybeanA){return new BeanE(mybeanA);}
    @Bean(name = "beanF")
    @Lazy
    public BeanF getBeanF(){return new BeanF("Bird",-1);}

    @Bean
    public  MyBeanPostProcessor getBean(){ return new MyBeanPostProcessor();}
}
