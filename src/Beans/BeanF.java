package Beans;

public class BeanF implements interfaceBeanValidator{

    private String name;
    private int value;

    public BeanF(String name, int value){
        this.name=name;
        this.value=value;

    }
    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate() {
        if(name==null){
            System.out.println("мале ім'я");
        }else if(value<=0){
            System.out.println("value мале");
        }else{
            System.out.println("ok!!!!");
        }
    }


}
