package Beans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

@Configuration
@PropertySource("my.properties")
public class BeanСonfigur {
    @Value("${BeanB.name}")
    private String nameB;
    @Value("${BeanB.value}")
    private String valueB;
    @Bean(value = "beanB", initMethod = "init", destroyMethod = "destroy")
    @DependsOn(value = "beanD")
    @Qualifier("mybeanB")
    public BeanB getBeanB(){return new BeanB(nameB,Integer.parseInt(valueB));
    }

    @Value("${BeanC.name}")
    private String nameC;
    @Value("${BeanC.value}")
    private String valueC;
   @Bean (name = "beanC" , initMethod = "init", destroyMethod = "destroy")
   @DependsOn(value = "beanB")
   @Qualifier("mybeanC")
   public BeanC getBeanC(){return new BeanC(nameC,Integer.parseInt(valueC));
   }


    @Value("${BeanD.name}")
    private String nameD;
    @Value("${BeanD.value}")
    private String valueD;
    @Bean (name = "beanD" , initMethod = "init", destroyMethod = "destroy")
    @Qualifier("mybeanD")
    public BeanD getBeanD(){return new BeanD(nameD,Integer.parseInt(valueD));
    }




}
