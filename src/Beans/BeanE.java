package Beans;

public class BeanE implements interfaceBeanValidator{

    private String name;
    private int value;

    public BeanE(BeanA beanA){
        this.name=beanA.getName();
        this.value=beanA.getValue();

    }
    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate() {
        if(name==null){
            System.out.println("мале ім'я");
        }else if(value<=0){
            System.out.println("value мале");
        }else{
            System.out.println("ok!!!!");
        }
    }
}
