package Beans;

public class BeanB implements interfaceBeanValidator{

    private String name;
    private int value;

    public BeanB(String name, int value){
        this.name=name;
        this.value=value;
        System.out.println("b created");
    }
    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate() {
        if(name==null){
            System.out.println("пусте ім'я");
        }else if(value<=0){
            System.out.println("value мале");
        }else{
            System.out.println("ok!!!!");
        }
    }
    public void init(){
        System.out.println("BeanB initialized");
    }
    public void destroy(){
        System.out.println("BeanB destroy");
    }
}
