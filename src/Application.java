import Beans.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;

public class Application {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigurImport.class);


        BeanA beanA = context.getBean(BeanA.class);
        System.out.println(beanA);
        BeanB beanB = context.getBean(BeanB.class);
        //System.out.println(beanB);
        BeanC beanC = context.getBean(BeanC.class);
        //System.out.println(beanC);
        BeanD beanD = context.getBean(BeanD.class);
        //System.out.println(beanD);
        BeanE beanE = context.getBean(BeanE.class);
        System.out.println(beanE);
        BeanF beanF = context.getBean(BeanF.class);
        System.out.println(beanF);
    }
}
